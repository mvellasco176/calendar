import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

import {Validators, FormBuilder, FormGroup } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-task',
  templateUrl: 'task.html',
})
export class TaskPage {

  newTask: FormGroup;
  day: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    public viewCtrl: ViewController,
    private formBuilder: FormBuilder) {

      this.day = this.navParams.get('day');
      this.newTask = this.formBuilder.group({
        id: 0,
        name: ['', Validators.required],
        time: ['', Validators.required],
      });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TaskPage');
  }

  onSubmit() {
    let element = this.day.tasks[this.day.tasks.length - 1];
    this.newTask.value.id = element.id + 1;
    this.day.tasks.push(this.newTask.value);
    this.day.show_tasks = false;
    this.day.save();
    this.day.show_tasks = true;
    this.viewCtrl.dismiss();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
