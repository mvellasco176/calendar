import { Component, OnInit, Inject } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';

import { TaskPage } from '../task/task';

import { Day } from '../../shared/day';
import { DayProvider } from '../../providers/day/day';


@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit{
  public days: Day[];
  public updateday = Day;
  errMess: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private dayService: DayProvider,
    public modalCtrl: ModalController,
    @Inject('BaseURL') private BaseURL){ }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }

  ngOnInit() {
    this.dayService.getDays().subscribe(
      days => this.days = days,
      errmess => this.errMess = <any>errmess
    );
  }

  openNewTask(selectedDay: Day) {
    let modal = this.modalCtrl.create(TaskPage, {day: selectedDay});
    modal.present();
  }

  updateTasks(selectedDay: Day) {
    this.dayService.getDay(selectedDay.id).subscribe(
      day => selectedDay.tasks = day.tasks,
      errmess => this.errMess = <any>errmess
    );
  }

  deleteTask(selectedDay: any, task: any) {
    let index = selectedDay.tasks.indexOf(task);
    if(index > -1) {
      selectedDay.tasks.splice(index, 1);
    }
    selectedDay.show_tasks = false;
    selectedDay.save();
    selectedDay.show_tasks = true;
  }

  toggle(selectedDay: Day) {
    selectedDay.show_tasks = !selectedDay.show_tasks;
    if(selectedDay.show_tasks){
      this.updateTasks(selectedDay);
    }
  }

}
