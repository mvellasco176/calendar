import { Task } from './task';

export class Day {
  id: number;
  name: string;
  abbr: string;
  date: string;
  color: string;
  show_tasks: boolean;
  tasks: Task[];
}
