import { Day } from './day';

export const DAYS: Day[] = [
  {
    id: 0,
    name: 'Monday',
    abbr:'mon',
    date: '04',
    color:'light',
    show_tasks: false,
    tasks: [
      { id: 0, name: 'Do Tasks', time: '2 pm' },
      { id: 1, name: 'Read Book', time: '5:30 pm' },
      { id: 2, name: 'Make Coffee', time: '6:30 pm' }
    ]},
  {
    id: 1,
    name: 'Yesterday',
    abbr:'tue',
    date: '05',
    color:'light',
    show_tasks: false,
    tasks: []},
  {
    id: 2,
    name: 'Today',
    abbr:'wed',
    date: '06',
    color:'primary',
    show_tasks: false,
    tasks: []},
  {
    id: 3,
    name: 'Tomorrow',
    abbr:'thu',
    date: '07',
    color:'primary',
    show_tasks: false,
    tasks: []},
  {
    id: 4,
    name: 'Friday',
    abbr:'fri',
    date: '08',
    color:'primary',
    show_tasks: false,
    tasks: []},
];
