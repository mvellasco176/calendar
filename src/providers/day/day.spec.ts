import { async, TestBed } from '@angular/core/testing';

import { Day } from '../../shared/day';
import { DAYS } from '../../shared/days';
import { DayProvider } from './day';

import { baseURL } from '../../shared/baseurl';
import { Observable } from 'rxjs/Observable';


describe('Day Provider Service', () => {
    beforeEach(async(() => {

       let dayServiceStub = {
         getDays: function(): Observable<Day[]> {
           return Observable.of(DAYS);
         }
       };

       TestBed.configureTestingModule({
         providers: [
           { provide: DayService, useValue: dayServiceStub },
           { provide: 'BaseURL', useValue: baseURL }
         ]
       }).compileComponents();

       let dayservice = TestBed.get(DayService);

    }));

    it('should return 5 days', () => {
        let result = dayProvider.getDays();

        expect(result.length).toEqual(5);
    });

});
