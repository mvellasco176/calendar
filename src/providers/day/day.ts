import { Injectable } from '@angular/core';
import { Day } from '../../shared/day';
import { Observable } from 'rxjs/Observable';
import { baseURL } from '../../shared/baseurl';
import { ProcessHttpmsgProvider } from '../process-httpmsg/process-httpmsg';
import { RestangularModule, Restangular } from 'ngx-restangular';


@Injectable()
export class DayProvider {

  constructor(private restangular: Restangular,
              private processHTTPMsgService: ProcessHttpmsgProvider) { }

  getDays(): Observable<Day[]> {
    return this.restangular.all('days').getList();
  }

  getDay(id: number): Observable<Day> {
    return this.restangular.one('days', id).get();
  }

}
