# Setup the project

npm install

# Start the json-server
In a separate terminal window do:
cd json-server
json-server --watch db.json -d 2000

## The tests are not working due to a bug in my version of webpack but you can try to run it using:
#For unit tests
npm run test
#For end2end tests
npm run e2e

# To start the ionic application use:
ionic serve --lab --no-open
